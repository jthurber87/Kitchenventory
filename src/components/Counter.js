import React, {useState} from "react";
import {Button} from "react-bootstrap";


function Counter() {
     const [counter, setCounter] = useState(1)
     return (
               <div className="plusMinus">
                    <Button className="button" onClick={() => {if(counter > 0){setCounter(counter-1)}}}>-</Button>
                    <p>{counter}</p>
                    <Button className="button" onClick={() => {if(counter <10){setCounter(counter+1)}}}>+</Button>
               </div>
     );
}

export default Counter;
