// "https://serpapi.com/search?q=dove%20soap&engine=google&gl=us&hl=en&tbm=isch&api_key=499c15b2fb2637e8f49b46caf01210aa21b88502611b4d2e6ec1b8c0cfdb6d89"

import React, {useState, useEffect} from "react";
import {Form, Button} from "react-bootstrap";

function SearchBar() {
     const [input, setInput] = useState("");

     useEffect(()=>{
          console.log(input)
     }, [input])

     const handleChange = (e) => {
         setInput(e.target.value)
     }

     return (
          <Form className="searchBar">
               <Form.Group className="mb-3" controlId="formBasicSearch">
                    <Form.Control type="search" value={input} onChange={handleChange} placeholder="Find product..." />
               </Form.Group>
               <Button>Submit</Button>
          </Form>
     );
}

export default SearchBar;
